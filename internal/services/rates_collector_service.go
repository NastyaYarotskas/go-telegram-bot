package services

import (
	"context"
	"go.uber.org/zap"
	"time"

	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

type RatesGetter interface {
	GetRates(ctx context.Context, date int64) ([]dto.Rate, error)
}

type RatesStorage interface {
	AddRate(ctx context.Context, rate dto.Rate) error
}

type SettingsGetter interface {
	GetSupportedCurrencies() []string
}

type RatesCollectorService struct {
	ratesGetter  RatesGetter
	ratesStorage RatesStorage
	config       SettingsGetter
	force        chan struct{}
	done         chan struct{}
}

func NewRatesCollectorService(ratesGetter RatesGetter, ratesStorage RatesStorage, config SettingsGetter) *RatesCollectorService {
	return &RatesCollectorService{
		ratesGetter:  ratesGetter,
		ratesStorage: ratesStorage,
		config:       config,
		force:        make(chan struct{}),
		done:         make(chan struct{}),
	}
}

func (s *RatesCollectorService) CollectRates(ctx context.Context) {
	ticker := time.NewTicker(10 * time.Minute)

	go func() {
		for {
			select {
			case <-s.force:
				zap.L().Debug("start collecting force rates")
				if err := s.FetchRates(ctx, time.Now().Unix()); err != nil {
					zap.L().Error("could not fetch rates", zap.Error(err))
				}
				ticker.Reset(10 * time.Minute)
				zap.L().Debug("finish collecting force rates")
				s.done <- struct{}{}
			case <-ticker.C:
				if err := s.FetchRates(ctx, time.Now().Unix()); err != nil {
					zap.L().Error("could not fetch rates", zap.Error(err))
				}
			case <-ctx.Done():
				zap.L().Info("stop receiving exchange rates")
				return
			}
		}
	}()
}

func (s *RatesCollectorService) ForceRatesUpdate() {
	s.force <- struct{}{}
	<-s.done
}

func (s *RatesCollectorService) FetchRates(ctx context.Context, date int64) error {
	rates, err := s.ratesGetter.GetRates(ctx, date)
	if err != nil {
		return err
	}

	filteredRates := make([]dto.Rate, 0, len(s.config.GetSupportedCurrencies()))
	supportedCurrenciesMap := buildSupportedCurrenciesMap(s.config.GetSupportedCurrencies())

	for _, rate := range rates {
		if _, ok := supportedCurrenciesMap[rate.Code]; ok {
			filteredRates = append(filteredRates, rate)
		}
	}

	zap.L().Debug("fetched rates", zap.Any("rates", filteredRates))

	for _, rate := range filteredRates {
		err = s.ratesStorage.AddRate(ctx, rate)
		if err != nil {
			return err
		}
	}

	return nil
}

func buildSupportedCurrenciesMap(supportedCurrencies []string) map[string]struct{} {
	supportedCurrenciesMap := make(map[string]struct{})

	for _, currency := range supportedCurrencies {
		supportedCurrenciesMap[currency] = struct{}{}
	}

	return supportedCurrenciesMap
}
