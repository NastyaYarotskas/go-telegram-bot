package config

import (
	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
)

func InitTracing(serviceName string) {
	cfg := config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
	}

	_, err := cfg.InitGlobalTracer(serviceName)
	if err != nil {
		zap.L().Fatal("cannot init tracing", zap.Error(err))
	}
}
