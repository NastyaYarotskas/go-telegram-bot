package config

import (
	"os"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

const configFile = "data/config.yaml"

type Config struct {
	Token               string   `yaml:"token"`
	SupportedCurrencies []string `yaml:"supportedCurrencies"`
	DefaultCurrency     string   `yaml:"defaultCurrency"`
	DevelMode           bool     `yaml:"develMode"`
	ServiceName         string   `yaml:"serviceName"`
}

type Service struct {
	config Config
}

func New() (*Service, error) {
	s := &Service{}

	rawYAML, err := os.ReadFile(configFile)
	if err != nil {
		return nil, errors.Wrap(err, "reading config file")
	}

	err = yaml.Unmarshal(rawYAML, &s.config)
	if err != nil {
		return nil, errors.Wrap(err, "parsing yaml")
	}

	return s, nil
}

func (s *Service) Token() string {
	return s.config.Token
}

func (s *Service) GetSupportedCurrencies() []string {
	return s.config.SupportedCurrencies
}

func (s *Service) GetDefaultCurrency() string {
	return s.config.DefaultCurrency
}

func (s *Service) GetDevelMode() bool {
	return s.config.DevelMode
}

func (s *Service) GetServiceName() string {
	return s.config.ServiceName
}
