package dto

import "github.com/shopspring/decimal"

type Rate struct {
	Code    string
	Nominal int64
	Value   decimal.Decimal
	Date    int64
}
