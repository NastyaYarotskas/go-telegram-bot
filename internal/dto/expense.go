package dto

import "github.com/shopspring/decimal"

type Expense struct {
	Amount   decimal.Decimal
	Category string
	Date     int64
}
