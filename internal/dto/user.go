package dto

import "github.com/shopspring/decimal"

type User struct {
	UserID        int64
	CurrencyCode  string
	MonthlyBudget decimal.Decimal
}
