package storage

import (
	"context"
	"database/sql"
	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
	"time"
)

const (
	addRateQuery = `
INSERT INTO rate(code, nominal, value, date)
VALUES ($1, $2, $3, $4)
ON CONFLICT (code, date) DO UPDATE
    SET nominal = EXCLUDED.nominal,
        value   = EXCLUDED.value;`

	getRateQuery = `
SELECT code, nominal, value, date
FROM rate
WHERE code = $1 AND date = $2;
`
)

type RateStorage struct {
	db *sql.DB
}

func NewRateStorage(db *sql.DB) *RateStorage {
	return &RateStorage{db: db}
}

func (s *RateStorage) AddRate(ctx context.Context, rate dto.Rate) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "add rate db")
	defer span.Finish()

	ts := time.Unix(rate.Date, 0).Format("2006-01-02")
	_, err := s.db.ExecContext(ctx, addRateQuery, rate.Code, rate.Nominal, rate.Value.String(), ts)
	return err
}

func (s *RateStorage) GetRate(ctx context.Context, code string, date int64) (*dto.Rate, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "get rate db")
	defer span.Finish()

	timestamp := time.Unix(date, 0).Format("2006-01-02")
	rows, err := s.db.QueryContext(ctx, getRateQuery, code, timestamp)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rateResult *dto.Rate

	for rows.Next() {
		var rate dto.Rate
		var ts time.Time
		err = rows.Scan(&rate.Code, &rate.Nominal, &rate.Value, &ts)
		if err != nil {
			return nil, err
		}

		rate.Date = ts.Unix()
		rateResult = &rate
	}

	return rateResult, nil
}
