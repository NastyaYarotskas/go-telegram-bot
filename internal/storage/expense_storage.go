package storage

import (
	"context"
	"database/sql"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
	"time"
)

const (
	addExpenseQuery = `
INSERT INTO expense(user_id, category, date, amount)
VALUES ($1, $2, $3, $4);
`
	getMonthlyExpensesQuery = `
SELECT amount
FROM expense
WHERE user_id = $1
  AND date >= date_trunc('month', $2::timestamp)
  AND date < date_trunc('month', $2::timestamp) + interval '1 month';
`
	getUserMonthlyBudgetQuery = `
SELECT budget
FROM "user"
WHERE id = $1;
`
	getExpensesByUserIDQuery = `
SELECT amount, category, date
FROM expense
WHERE user_id = $1
`
)

type ExpenseStorage struct {
	db *sql.DB
}

func NewExpenseStorage(db *sql.DB) *ExpenseStorage {
	return &ExpenseStorage{db: db}
}

func (s *ExpenseStorage) AddExpense(ctx context.Context, userID int64, expense dto.Expense) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "add expense db")
	defer span.Finish()

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	// Defer a rollback in case anything fails.
	defer tx.Rollback() // nolint:errcheck

	ts := time.Unix(expense.Date, 0).Format("2006-01-02")
	_, err = tx.ExecContext(ctx, addExpenseQuery, userID, expense.Category, ts, expense.Amount)
	if err != nil {
		return err
	}

	rows, err := tx.QueryContext(ctx, getMonthlyExpensesQuery, userID, ts)
	if err != nil {
		return err
	}
	defer rows.Close()

	var amount decimal.Decimal
	sum := decimal.NewFromInt(0)

	for rows.Next() {
		err = rows.Scan(&amount)
		if err != nil {
			return err
		}

		sum = sum.Add(amount)
	}

	var budget decimal.Decimal
	err = tx.QueryRowContext(ctx, getUserMonthlyBudgetQuery, userID).Scan(&budget)
	if err != nil {
		return err
	}

	if sum.GreaterThan(budget) && !budget.IsZero() {
		return errors.New("Budget overrun.")
	}

	return tx.Commit()
}

func (s *ExpenseStorage) GetExpensesByUserID(ctx context.Context, userID int64) ([]dto.Expense, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "get expenses by user id db")
	defer span.Finish()

	rows, err := s.db.QueryContext(ctx, getExpensesByUserIDQuery, userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	expenses := make([]dto.Expense, 0)

	for rows.Next() {
		var expense dto.Expense
		var ts time.Time
		err = rows.Scan(&expense.Amount, &expense.Category, &ts)
		if err != nil {
			return nil, err
		}

		expense.Date = ts.Unix()
		expenses = append(expenses, expense)
	}

	return expenses, nil
}
