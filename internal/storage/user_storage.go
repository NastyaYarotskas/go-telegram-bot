package storage

import (
	"context"
	"database/sql"
	"github.com/opentracing/opentracing-go"
	"github.com/shopspring/decimal"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

const (
	getUserQuery = `
SELECT id, currency, budget
FROM "user"
WHERE id = $1
`
	addUserQuery = `
INSERT INTO "user"(id, currency, budget)
VALUES ($1, $2, $3)
`
	updateCurrencyQuery = `
UPDATE "user"
SET currency = $2
WHERE id = $1
`
	updateBudgetQuery = `
UPDATE "user"
SET budget = $2
WHERE id = $1
`
)

type UserStorage struct {
	db *sql.DB
}

func NewUserStorage(db *sql.DB) *UserStorage {
	return &UserStorage{db: db}
}

func (s *UserStorage) GetUser(ctx context.Context, userID int64) (dto.User, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "get user db")
	defer span.Finish()

	var user dto.User
	err := s.db.QueryRowContext(ctx, getUserQuery, userID).Scan(&user.UserID, &user.CurrencyCode, &user.MonthlyBudget)
	return user, err
}

func (s *UserStorage) AddUser(ctx context.Context, user dto.User) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "add user db")
	defer span.Finish()

	_, err := s.db.ExecContext(ctx, addUserQuery, user.UserID, user.CurrencyCode, user.MonthlyBudget)
	return err
}

func (s *UserStorage) UpdateCurrencyCode(ctx context.Context, userID int64, code string) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "update currency code db")
	defer span.Finish()

	_, err := s.db.ExecContext(ctx, updateCurrencyQuery, userID, code)
	return err
}

func (s *UserStorage) UpdateMonthlyBudget(ctx context.Context, userID int64, budget decimal.Decimal) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "update monthly budget db")
	defer span.Finish()

	_, err := s.db.ExecContext(ctx, updateBudgetQuery, userID, budget)
	return err
}
