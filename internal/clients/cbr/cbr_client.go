package cbr

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/shopspring/decimal"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

const (
	AcceptHeader            = "Accept"
	ContentTypeHeader       = "Content-Type"
	ApplicationJsonProperty = "application/json"

	cbrURL = "https://www.cbr-xml-daily.ru/archive/%s/daily_json.js"
)

type RatesDto struct {
	Currency map[string]CurrencyDto `json:"Valute"`
}

type CurrencyDto struct {
	Code    string          `json:"CharCode"`
	Nominal int64           `json:"Nominal"`
	Value   decimal.Decimal `json:"Value"`
}

type Client struct {
	inner *resty.Client
}

func NewClient() *Client {
	client := resty.New()

	return &Client{
		inner: client,
	}
}

func (c *Client) GetRates(ctx context.Context, date int64) ([]dto.Rate, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	var ratesDto RatesDto

	url := fmt.Sprintf(cbrURL, time.Unix(date, 0).AddDate(0, 0, -1).Format("2006/01/02"))

	response, err := c.inner.R().
		SetContext(ctx).
		SetHeader(AcceptHeader, ApplicationJsonProperty).
		SetHeader(ContentTypeHeader, ApplicationJsonProperty).
		Get(url)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(response.Body(), &ratesDto)
	if err != nil {
		return nil, err
	}

	rates := make([]dto.Rate, 0, len(ratesDto.Currency))

	for _, currency := range ratesDto.Currency {
		rate := dto.Rate{
			Code:    currency.Code,
			Nominal: currency.Nominal,
			Value:   currency.Value,
			Date:    date,
		}

		rates = append(rates, rate)
	}

	return rates, nil
}
