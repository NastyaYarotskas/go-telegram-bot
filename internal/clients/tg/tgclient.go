package tg

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/model/messages"
	"go.uber.org/zap"
	"strings"
	"time"
)

var (
	RequestsTotalCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "tg",
		Name:      "requests_total",
	}, []string{"command"})

	ResponseTimeSecondsByCommand = promauto.NewSummaryVec(prometheus.SummaryOpts{
		Namespace: "ozon",
		Subsystem: "tg",
		Name:      "summary_response_time_seconds",
		Objectives: map[float64]float64{
			0.5:  0.1,
			0.9:  0.01,
			0.99: 0.001,
		},
	}, []string{"command"})
)

type TokenGetter interface {
	Token() string
}

type Client struct {
	client *tgbotapi.BotAPI
}

func New(tokenGetter TokenGetter) (*Client, error) {
	client, err := tgbotapi.NewBotAPI(tokenGetter.Token())
	if err != nil {
		return nil, errors.Wrap(err, "NewBotAPI")
	}

	return &Client{
		client: client,
	}, nil
}

func (c *Client) SendMessage(text string, userID int64) error {
	_, err := c.client.Send(tgbotapi.NewMessage(userID, text))
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) ListenUpdates(ctx context.Context, msgModel *messages.Model) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := c.client.GetUpdatesChan(u)

	zap.L().Info("start listening tg updates")

	go func() {
		<-ctx.Done()
		c.client.StopReceivingUpdates()
		zap.L().Info("stop receiving tg updates")
	}()

	for update := range updates {
		if update.Message != nil { // If we got a message
			span, ctx := opentracing.StartSpanFromContext(ctx, "incoming message")

			zap.L().Info("got new message",
				zap.String("user", update.Message.From.UserName),
				zap.String("message", update.Message.Text))

			command := strings.Split(update.Message.Text, " ")

			startTime := time.Now()

			err := msgModel.IncomingMessage(ctx, messages.Message{
				Text:   update.Message.Text,
				UserID: update.Message.From.ID,
			})
			if err != nil {
				zap.L().Info("error processing message", zap.Error(err))
			}

			duration := time.Since(startTime)

			RequestsTotalCount.WithLabelValues(command[0]).Add(1)
			ResponseTimeSecondsByCommand.WithLabelValues(command[0]).Observe(duration.Seconds())

			span.Finish()
		}
	}
}
