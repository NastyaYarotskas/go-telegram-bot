package messages

import (
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"strings"

	"github.com/enescakir/emoji"
)

func (s *Model) executeChangeCurrency(ctx context.Context, msg Message) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "currency command")
	defer span.Finish()

	args := strings.Split(msg.Text, " ")

	if len(args) != 2 {
		return "", &CommandExecutionError{
			message: "Wrong number of arguments. Command must contains 2 arguments.",
		}
	}

	supportedCurrenciesMap := buildSupportedCurrenciesMap(s.config.GetSupportedCurrencies())

	currencyCode := args[1]
	if _, ok := supportedCurrenciesMap[currencyCode]; !ok {
		return "", &CommandExecutionError{
			message: "Unsupported currency.",
		}
	}

	err := s.userStorage.UpdateCurrencyCode(ctx, msg.UserID, currencyCode)
	if err != nil {
		return "", &CommandExecutionError{
			message: "Problems with updating currency code. Please try again latter.",
		}
	}

	return fmt.Sprintf("%s Changed currency to %s", emoji.CheckMarkButton, currencyCode), nil
}

func buildSupportedCurrenciesMap(supportedCurrencies []string) map[string]struct{} {
	supportedCurrenciesMap := make(map[string]struct{})

	for _, currency := range supportedCurrencies {
		supportedCurrenciesMap[currency] = struct{}{}
	}

	return supportedCurrenciesMap
}
