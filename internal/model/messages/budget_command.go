package messages

import (
	"context"
	"fmt"
	"github.com/enescakir/emoji"
	"github.com/opentracing/opentracing-go"
	"github.com/shopspring/decimal"
	"go.uber.org/zap"
	"strings"
	"time"
)

func (s *Model) executeBudgetCommand(ctx context.Context, msg Message) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "budget command")
	defer span.Finish()

	args := strings.Split(msg.Text, " ")

	if len(args) != 2 {
		return "", &CommandExecutionError{
			message: "Wrong number of arguments. Command must contains 2 arguments.",
		}
	}

	if args[1] == "0" {
		return "", &CommandExecutionError{
			message: "The budget can't be zero.",
		}
	}

	if args[1] == "disable" {
		args[1] = "0"
	}

	budget, err := decimal.NewFromString(args[1])
	if err != nil {
		zap.L().Error("wrong budget format", zap.Error(err), zap.String("budget", args[1]))
		return "", &CommandExecutionError{
			message: fmt.Sprintf("The budget %s was entered in the wrong format.", args[1]),
		}
	}

	user, err := s.userStorage.GetUser(ctx, msg.UserID)
	if err != nil {
		zap.L().Error("user not found", zap.Error(err), zap.Int64p("userId", &msg.UserID))
		return "", &CommandExecutionError{
			message: "Cannot get user",
		}
	}

	var recalcBudget decimal.Decimal

	if user.CurrencyCode != s.config.GetDefaultCurrency() {
		t, _ := time.Parse(dateLayout, time.Now().Format(dateLayout))
		rate, err := s.ratesStorage.GetRate(ctx, user.CurrencyCode, t.Unix())
		if err != nil {
			zap.L().Error("could not fetch rates", zap.Error(err), zap.String("code", user.CurrencyCode))
			return "", &CommandExecutionError{
				message: "Problems with fetching rates. Please try again letter.",
			}
		}

		if rate == nil {
			s.ratesGetter.ForceRatesUpdate()

			rate, err = s.ratesStorage.GetRate(ctx, user.CurrencyCode, t.Unix())
			if err != nil || rate == nil {
				zap.L().Error("could not fetch rates", zap.Error(err), zap.String("code", user.CurrencyCode))
				return "", &CommandExecutionError{
					message: "Problems with fetching rates. Please again try letter.",
				}
			}
		}

		nominal := decimal.NewFromInt(rate.Nominal)
		recalcBudget = budget.Mul(nominal).Mul(rate.Value)
	} else {
		recalcBudget = budget
	}

	err = s.userStorage.UpdateMonthlyBudget(ctx, msg.UserID, recalcBudget)
	if err != nil {
		zap.L().Error("could not update monthly budget", zap.Error(err),
			zap.Int64p("userId", &msg.UserID), zap.Any("budget", recalcBudget))
		return "", &CommandExecutionError{
			message: "Problems with updating budget. Please try again letter.",
		}
	}

	if budget.IsZero() {
		return fmt.Sprintf("%s Disable monthly budget", emoji.MoneyBag), nil
	}

	return fmt.Sprintf("%s Updated monthly budget to the %s", emoji.MoneyBag, budget), nil
}
