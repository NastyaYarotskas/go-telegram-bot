package messages

import (
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"strings"
	"time"

	"github.com/enescakir/emoji"
	"github.com/shopspring/decimal"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

func (s *Model) executeAddCommand(ctx context.Context, msg Message) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "add command")
	defer span.Finish()

	args := strings.Split(msg.Text, " ")

	if len(args) != 4 {
		return "", &CommandExecutionError{
			message: "Wrong number of arguments. Command must contains 3 arguments.",
		}
	}

	category := args[1]
	price, err := decimal.NewFromString(args[2])
	if err != nil {
		zap.L().Error("wrong price format", zap.Error(err), zap.String("price", args[2]))
		return "", &CommandExecutionError{
			message: fmt.Sprintf("The price %s was entered in the wrong format.", args[2]),
		}
	}

	date, err := time.Parse(dateLayout, args[3])
	if err != nil {
		zap.L().Error("wrong date format", zap.Error(err), zap.String("date", args[3]))
		return "", &CommandExecutionError{
			message: fmt.Sprintf("The date %s was entered in the wrong format. The date must be in the form yyyy.mm.dd.", args[3]),
		}
	}

	user, err := s.userStorage.GetUser(ctx, msg.UserID)
	if err != nil {
		zap.L().Error("user not found", zap.Error(err), zap.Int64p("userId", &msg.UserID))
		return "", &CommandExecutionError{
			message: "Cannot get user",
		}
	}

	var amount decimal.Decimal

	if user.CurrencyCode != s.config.GetDefaultCurrency() {
		rate, err := s.ratesStorage.GetRate(ctx, user.CurrencyCode, date.Unix())
		if err != nil {
			zap.L().Error("could not fetch rates", zap.Error(err), zap.String("code", user.CurrencyCode))
			return "", &CommandExecutionError{
				message: "Problems with fetching rates. Please try letter.",
			}
		}

		if rate == nil {
			s.ratesGetter.ForceRatesUpdate()

			rate, err = s.ratesStorage.GetRate(ctx, user.CurrencyCode, date.Unix())
			if err != nil || rate == nil {
				zap.L().Error("could not fetch rates", zap.Error(err), zap.String("code", user.CurrencyCode))
				return "", &CommandExecutionError{
					message: "Problems with fetching rates. Please try letter.",
				}
			}
		}

		nominal := decimal.NewFromInt(rate.Nominal)
		amount = price.Mul(nominal).Mul(rate.Value)
	} else {
		amount = price
	}

	expense := dto.Expense{
		Amount:   amount,
		Category: category,
		Date:     date.Unix(),
	}

	err = s.expenseStorage.AddExpense(ctx, msg.UserID, expense)
	if err != nil {
		zap.L().Error("could not save expense in db", zap.Error(err),
			zap.Int64p("userId", &msg.UserID), zap.Any("expense", expense))
		return "", &CommandExecutionError{
			message: fmt.Sprintf("Problems with adding expenses. %s Please try letter.", err.Error()),
		}
	}

	return fmt.Sprintf("%s Added %s to the %s %v", emoji.CheckMarkButton,
		price.StringFixed(2), category, date.Format(dateLayout)), nil
}
