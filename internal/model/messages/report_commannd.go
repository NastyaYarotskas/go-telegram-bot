package messages

import (
	"bytes"
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"sort"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/enescakir/emoji"
	"github.com/shopspring/decimal"
)

func (s *Model) executeGetReportCommand(ctx context.Context, msg Message) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "report command")
	defer span.Finish()

	args := strings.Split(msg.Text, " ")

	if len(args) != 2 {
		return "", &CommandExecutionError{
			message: "Wrong number of arguments. Command must contains 1 argument.",
		}
	}

	period := args[1]

	now := time.Now()

	var beforeTime time.Time
	switch period {
	case "week":
		beforeTime = now.AddDate(0, 0, -7)
	case "month":
		beforeTime = now.AddDate(0, -1, 0)
	case "year":
		beforeTime = now.AddDate(-1, 0, 0)
	default:
		return "", &CommandExecutionError{
			message: fmt.Sprintf("%s can't be used as a period for report generation.", period),
		}
	}

	user, err := s.userStorage.GetUser(ctx, msg.UserID)
	if err != nil {
		zap.L().Error("user not found", zap.Error(err), zap.Int64p("userId", &msg.UserID))
		return "", &CommandExecutionError{
			message: "Cannot get user",
		}
	}

	expenses, err := s.expenseStorage.GetExpensesByUserID(ctx, msg.UserID)
	if err != nil {
		zap.L().Error("could not fetch expenses", zap.Error(err), zap.Int64p("userId", &msg.UserID))
		return "", &CommandExecutionError{
			message: "Cannot get expenses",
		}
	}

	defaultCurrency := s.config.GetDefaultCurrency()

	categoryToAmount := make(map[string]decimal.Decimal, 0)
	for _, expense := range expenses {
		if expense.Date > beforeTime.Unix() {

			var amount decimal.Decimal

			if user.CurrencyCode != defaultCurrency {
				rate, err := s.ratesStorage.GetRate(ctx, user.CurrencyCode, expense.Date)
				if err != nil {
					zap.L().Error("could not fetch rates", zap.Error(err),
						zap.Int64p("userId", &msg.UserID), zap.String("code", user.CurrencyCode))
					return "", &CommandExecutionError{
						message: "Problems with fetching rates. Please try letter.",
					}
				}

				if rate == nil {
					s.ratesGetter.ForceRatesUpdate()

					rate, err = s.ratesStorage.GetRate(ctx, user.CurrencyCode, expense.Date)
					if err != nil || rate == nil {
						zap.L().Error("could not fetch rates", zap.Error(err),
							zap.Int64p("userId", &msg.UserID), zap.String("code", user.CurrencyCode))
						return "", &CommandExecutionError{
							message: "Problems with fetching rates. Please try letter.",
						}
					}
				}

				nominal := decimal.NewFromInt(rate.Nominal)
				amount = expense.Amount.Div(nominal).Div(rate.Value).Round(2)
			} else {
				amount = expense.Amount
			}

			if _, ok := categoryToAmount[expense.Category]; !ok {
				categoryToAmount[expense.Category] = amount
			} else {
				categoryToAmount[expense.Category] = categoryToAmount[expense.Category].Add(amount)
			}
		}
	}

	categories := make([]string, 0)
	for category := range categoryToAmount {
		categories = append(categories, category)
	}

	sort.Strings(categories)

	report, err := generateReport(categoryToAmount, categories, period)
	if err != nil {
		zap.L().Error("could not generate report", zap.Int64p("userId", &msg.UserID))
		return "", &CommandExecutionError{message: "Problems with report generation."}
	}

	return report, nil
}

func generateReport(categoryToAmount map[string]decimal.Decimal, categories []string, period string) (string, error) {
	var report string

	var b bytes.Buffer

	w := tabwriter.NewWriter(&b, 1, 1, 1, ' ', 0)

	for _, category := range categories {
		amount := categoryToAmount[category].StringFixed(2)
		_, err := fmt.Fprintf(w, "%s\t%s\t\n", strings.ToUpper(category), amount)
		if err != nil {
			return "", err
		}
	}
	err := w.Flush()
	if err != nil {
		return "", err
	}

	report += fmt.Sprintf("%s Report for the last %s\n", emoji.TearOffCalendar.String(), period)
	report += "-------------------------------------------\n"
	report += b.String()
	report += "-------------------------------------------\n"

	return report, nil
}
