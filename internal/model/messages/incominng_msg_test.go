package messages

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
	mocks "gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/mocks/messages"
)

var (
	defaultCurrency     = "RUB"
	supportedCurrencies = []string{"USD", "CNY", "EUR", "RUB"}
)

func Test_OnStartCommand_ShouldAnswerWithIntroMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	config.EXPECT().GetSupportedCurrencies().Return(supportedCurrencies)
	userStorage.EXPECT().AddUser(gomock.Any(), gomock.Any())
	sender.EXPECT().SendMessage(fmt.Sprintf(welcomeMessage, supportedCurrencies, defaultCurrency), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/start",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnUnknownCommand_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Unknown command."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "some text",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommand_ShouldAddNewExpense(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: defaultCurrency}, nil)
	expenseStorage.EXPECT().AddExpense(gomock.Any(), int64(123), gomock.Any())
	sender.EXPECT().SendMessage("✅ Added 100.20 to the food 2022.12.22", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 100.20 2022.12.22",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandWithoutArguments_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Wrong number of arguments. Command must contains 3 arguments."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandWithWrongPriceFormat_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "The price 22,2 was entered in the wrong format."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 22,2 2022.02.02",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandWithWrongDateFormat_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "The date 2022/02/02 was entered in the wrong format. The date must be in the form yyyy.mm.dd."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 22.2 2022/02/02",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandWithUserSetNotDefCurrency_ShouldAddNewExpense(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	val, _ := decimal.NewFromString("61.2475")
	date, _ := time.Parse(dateLayout, "2022.12.22")
	rate := dto.Rate{Code: "USD", Nominal: 1, Value: val, Date: date.Unix()}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "USD"}, nil)
	ratesStorage.EXPECT().GetRate(gomock.Any(), "USD", gomock.Any()).Return(&rate, nil)
	expenseStorage.EXPECT().AddExpense(gomock.Any(), int64(123), gomock.Any())
	sender.EXPECT().SendMessage("✅ Added 100.20 to the food 2022.12.22", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 100.20 2022.12.22",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandWithEmptyRates_ShouldAddNewExpense(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	val, _ := decimal.NewFromString("61.2475")
	date, _ := time.Parse(dateLayout, "2022.12.22")
	rate := dto.Rate{Code: "USD", Nominal: 1, Value: val, Date: date.Unix()}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "USD"}, nil)
	ratesStorage.EXPECT().GetRate(gomock.Any(), "USD", gomock.Any()).Return(nil, nil)
	ratesGetter.EXPECT().ForceRatesUpdate()
	ratesStorage.EXPECT().GetRate(gomock.Any(), "USD", gomock.Any()).Return(&rate, nil)
	expenseStorage.EXPECT().AddExpense(gomock.Any(), int64(123), gomock.Any())
	sender.EXPECT().SendMessage("✅ Added 100.20 to the food 2022.12.22", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 100.20 2022.12.22",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnAddCommandBudgetOverrun_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: defaultCurrency}, nil)
	expenseStorage.EXPECT().AddExpense(gomock.Any(), int64(123), gomock.Any()).Return(errors.New("Budget overrun."))
	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Problems with adding expenses. Budget overrun. Please try letter."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/add food 100.20 2022.12.22",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandForWeekPeriod_ShouldAnswerWithReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	expenses := []dto.Expense{
		{Amount: decimal.NewFromInt(100), Category: "food", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(100), Category: "home", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, 0, -6).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "home", Date: time.Now().AddDate(0, 0, -6).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, -1, 0).Unix()},
	}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	expenseStorage.EXPECT().GetExpensesByUserID(gomock.Any(), int64(123)).Return(expenses, nil)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "RUB"}, nil)

	expectedMessage := `📆 Report for the last week
-------------------------------------------
FOOD 300.00 
HOME 300.00 
-------------------------------------------
`
	sender.EXPECT().SendMessage(expectedMessage, int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report week",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandForMonthPeriod_ShouldAnswerWithReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	expenses := []dto.Expense{
		{Amount: decimal.NewFromInt(100), Category: "food", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(100), Category: "home", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, 0, -10).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "home", Date: time.Now().AddDate(0, 0, -20).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, -1, -3).Unix()},
	}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	expenseStorage.EXPECT().GetExpensesByUserID(gomock.Any(), int64(123)).Return(expenses, nil)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "RUB"}, nil)

	expectedMessage := `📆 Report for the last month
-------------------------------------------
FOOD 300.00 
HOME 300.00 
-------------------------------------------
`
	sender.EXPECT().SendMessage(expectedMessage, int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report month",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandForYearPeriod_ShouldAnswerWithReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	expenses := []dto.Expense{
		{Amount: decimal.NewFromInt(100), Category: "food", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(100), Category: "home", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, 0, -10).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "home", Date: time.Now().AddDate(0, 0, -20).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(-1, -1, -3).Unix()},
	}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	expenseStorage.EXPECT().GetExpensesByUserID(gomock.Any(), int64(123)).Return(expenses, nil)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "RUB"}, nil)

	expectedMessage := `📆 Report for the last year
-------------------------------------------
FOOD 300.00 
HOME 300.00 
-------------------------------------------
`
	sender.EXPECT().SendMessage(expectedMessage, int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report year",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommand_ShouldAnswerWithEmptyReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	expenses := []dto.Expense{
		{Amount: decimal.NewFromInt(100), Category: "food", Date: time.Now().AddDate(0, -1, -1).Unix()},
		{Amount: decimal.NewFromInt(100), Category: "home", Date: time.Now().AddDate(0, -1, -1).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, -1, -6).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "home", Date: time.Now().AddDate(0, -1, -6).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, -1, 0).Unix()},
	}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	expenseStorage.EXPECT().GetExpensesByUserID(gomock.Any(), int64(123)).Return(expenses, nil)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "RUB"}, nil)

	expectedMessage := `📆 Report for the last week
-------------------------------------------
-------------------------------------------
`
	sender.EXPECT().SendMessage(expectedMessage, int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report week",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandWithNNotDefaultCurrency_ShouldAnswerWithReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	expenses := []dto.Expense{
		{Amount: decimal.NewFromInt(100), Category: "food", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(100), Category: "home", Date: time.Now().AddDate(0, 0, -1).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, 0, -10).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "home", Date: time.Now().AddDate(0, 0, -20).Unix()},
		{Amount: decimal.NewFromInt(200), Category: "food", Date: time.Now().AddDate(0, -1, -3).Unix()},
	}

	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	expenseStorage.EXPECT().GetExpensesByUserID(gomock.Any(), int64(123)).Return(expenses, nil)
	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "USD"}, nil)

	val, _ := decimal.NewFromString("61.2475")
	date, _ := time.Parse(dateLayout, "2022.12.22")
	rate := dto.Rate{Code: "USD", Nominal: 1, Value: val, Date: date.Unix()}

	ratesStorage.EXPECT().GetRate(gomock.Any(), "USD", gomock.Any()).Return(&rate, nil).AnyTimes()

	expectedMessage := `📆 Report for the last month
-------------------------------------------
FOOD 4.90 
HOME 4.90 
-------------------------------------------
`
	sender.EXPECT().SendMessage(expectedMessage, int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report month",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandWithoutPeriod_ShouldAnswerWithEmptyReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Wrong number of arguments. Command must contains 1 argument."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnGetReportCommandWithInvalidPeriod_ShouldAnswerWithEmptyReportMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "day can't be used as a period for report generation."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/report day",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnChangeCurrencyCommand_ShouldUpdateUserCurrencySettings(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	config.EXPECT().GetSupportedCurrencies().Return(supportedCurrencies)
	userStorage.EXPECT().UpdateCurrencyCode(gomock.Any(), int64(123), "EUR")
	sender.EXPECT().SendMessage("✅ Changed currency to EUR", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/currency EUR",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnChangeCurrencyCommandWithWrongNumOfArgs_ShouldAnswerHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Wrong number of arguments. Command must contains 2 arguments."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/currency EUR 13",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnChangeCurrencyCommandWithUnsupportedCurrencyCode_ShouldAnswerHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	config.EXPECT().GetSupportedCurrencies().Return(supportedCurrencies)
	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Unsupported currency."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/currency BY",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommand_ShouldUpdateUserBudgetSettings(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{
		UserID:        int64(123),
		CurrencyCode:  defaultCurrency,
		MonthlyBudget: decimal.Zero,
	}, nil)
	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().UpdateMonthlyBudget(gomock.Any(), int64(123), decimal.NewFromInt(100))
	sender.EXPECT().SendMessage("💰 Updated monthly budget to the 100", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget 100",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommandWithWrongNumOfArgs_ShouldAnswerHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "Wrong number of arguments. Command must contains 2 arguments."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget 100 100",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommandWithInvalidArgument_ShouldAnswerHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "The budget INF was entered in the wrong format."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget INF",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommandWithUserSetNotDefCurrency_ShouldUpdateUserBudgetSettings(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	val, _ := decimal.NewFromString("61.2475")
	date, _ := time.Parse(dateLayout, "2022.12.22")
	rate := dto.Rate{Code: "USD", Nominal: 1, Value: val, Date: date.Unix()}

	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "USD"}, nil)
	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	ratesStorage.EXPECT().GetRate(gomock.Any(), "USD", gomock.Any()).Return(&rate, nil)
	userStorage.EXPECT().UpdateMonthlyBudget(gomock.Any(), int64(123), gomock.Any())
	sender.EXPECT().SendMessage("💰 Updated monthly budget to the 100.2", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget 100.20",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommandWithNONEValue_ShouldUpdateUserBudgetSettings(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	userStorage.EXPECT().GetUser(gomock.Any(), int64(123)).Return(dto.User{UserID: int64(123), CurrencyCode: "RUB"}, nil)
	config.EXPECT().GetDefaultCurrency().Return(defaultCurrency)
	userStorage.EXPECT().UpdateMonthlyBudget(gomock.Any(), int64(123), gomock.Any())
	sender.EXPECT().SendMessage("💰 Disable monthly budget", int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget disable",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func Test_OnBudgetCommandWithZeroValue_ShouldAnswerWithHelpMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	sender := mocks.NewMockMessageSender(ctrl)
	expenseStorage := mocks.NewMockExpenseStorage(ctrl)
	userStorage := mocks.NewMockUserStorage(ctrl)
	ratesStorage := mocks.NewMockRatesStorage(ctrl)
	config := mocks.NewMockSettingsGetter(ctrl)
	ratesGetter := mocks.NewMockRatesGetter(ctrl)

	model := New(sender, expenseStorage, userStorage, ratesStorage, config, ratesGetter)

	sender.EXPECT().SendMessage(fmt.Sprintf(helpMessage, "The budget can't be zero."), int64(123))

	err := model.IncomingMessage(context.Background(), Message{
		Text:   "/budget 0",
		UserID: 123,
	})

	assert.NoError(t, err)
}
