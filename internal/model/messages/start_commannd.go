package messages

import (
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"

	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

func (s *Model) executeStartCommand(ctx context.Context, msg Message) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "start command")
	defer span.Finish()

	defaultCurrency := s.config.GetDefaultCurrency()

	newUser := dto.User{
		UserID:       msg.UserID,
		CurrencyCode: defaultCurrency,
	}

	err := s.userStorage.AddUser(ctx, newUser)
	if err != nil {
		return "", &CommandExecutionError{
			message: "Problems with adding user. Please try letter.",
		}
	}

	return fmt.Sprintf(welcomeMessage, s.config.GetSupportedCurrencies(), defaultCurrency), nil
}
