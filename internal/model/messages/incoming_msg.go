package messages

import (
	"context"
	"fmt"
	"github.com/shopspring/decimal"
	"regexp"

	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/dto"
)

var (
	startCommandPattern          = regexp.MustCompile(`/start`)
	addCommandPattern            = regexp.MustCompile(`/add`)
	reportCommandPattern         = regexp.MustCompile(`/report`)
	changeCurrencyCommandPattern = regexp.MustCompile(`/currency`)
	budgetCommandPattern         = regexp.MustCompile(`/budget`)
)

const dateLayout = "2006.01.02"

const welcomeMessage = `
This bot will help you store and visualize your expenses!
Bot commands:
*/add <category> <amount> <date>* will add new expense on date(yyyy.mm.dd) (example: ` + "`/add taxi 200.20 2006.02.01)`" + `
*/report <period>* will show your expenses for the specified period(week, month, year) (example: ` + "`/report month)`" + `
*/currency <code>* will change currency, available currencies codes: %v, by default %s, (example: ` + "`/currency EUR)`" + `
*/budget <amount>* will set monthly limit, if you enter instead amount *disable*, then budget will be disabled, (example: ` + "`/budget 100.50)`" + `
`

const helpMessage = `
%s

To see available bot command print /start
`

type CommandExecutionError struct {
	message string
}

func (e *CommandExecutionError) Error() string {
	return e.message
}

type MessageSender interface {
	SendMessage(text string, userID int64) error
}

type ExpenseStorage interface {
	AddExpense(ctx context.Context, userID int64, expense dto.Expense) error
	GetExpensesByUserID(ctx context.Context, userID int64) ([]dto.Expense, error)
}

type UserStorage interface {
	AddUser(ctx context.Context, user dto.User) error
	UpdateCurrencyCode(ctx context.Context, userID int64, code string) error
	GetUser(ctx context.Context, userID int64) (dto.User, error)
	UpdateMonthlyBudget(ctx context.Context, userID int64, budget decimal.Decimal) error
}

type RatesStorage interface {
	AddRate(ctx context.Context, rate dto.Rate) error
	GetRate(ctx context.Context, code string, date int64) (*dto.Rate, error)
}

type SettingsGetter interface {
	GetSupportedCurrencies() []string
	GetDefaultCurrency() string
}

type RatesGetter interface {
	ForceRatesUpdate()
}

type Model struct {
	tgClient       MessageSender
	expenseStorage ExpenseStorage
	userStorage    UserStorage
	ratesStorage   RatesStorage
	config         SettingsGetter
	ratesGetter    RatesGetter
}

func New(tgClient MessageSender, expenseStorage ExpenseStorage, userStorage UserStorage, ratesStorage RatesStorage, config SettingsGetter, ratesGetter RatesGetter) *Model {
	return &Model{
		tgClient:       tgClient,
		expenseStorage: expenseStorage,
		userStorage:    userStorage,
		ratesStorage:   ratesStorage,
		config:         config,
		ratesGetter:    ratesGetter,
	}
}

type Message struct {
	Text   string
	UserID int64
}

func (s *Model) IncomingMessage(ctx context.Context, msg Message) error {
	var (
		answer string
		err    error
	)

	switch {
	case startCommandPattern.Match([]byte(msg.Text)):
		answer, err = s.executeStartCommand(ctx, msg)
	case addCommandPattern.Match([]byte(msg.Text)):
		answer, err = s.executeAddCommand(ctx, msg)
	case reportCommandPattern.Match([]byte(msg.Text)):
		answer, err = s.executeGetReportCommand(ctx, msg)
	case changeCurrencyCommandPattern.Match([]byte(msg.Text)):
		answer, err = s.executeChangeCurrency(ctx, msg)
	case budgetCommandPattern.Match([]byte(msg.Text)):
		answer, err = s.executeBudgetCommand(ctx, msg)
	default:
		err = &CommandExecutionError{
			message: "Unknown command.",
		}
	}

	if err != nil {
		answer = fmt.Sprintf(helpMessage, err.Error())
	}

	return s.tgClient.SendMessage(answer, msg.UserID)
}
