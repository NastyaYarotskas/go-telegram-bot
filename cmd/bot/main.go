package main

import (
	"context"
	"database/sql"
	"fmt"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"

	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/clients/cbr"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/clients/tg"
	conf "gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/config"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/model/messages"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/services"
	"gitlab.ozon.dev/nastyayarotskas/telegram-bot/internal/storage"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	config, err := conf.New()
	if err != nil {
		log.Fatal("config init failed:", err)
	}

	conf.InitLogger(config.GetDevelMode())
	conf.InitTracing(config.GetServiceName())

	http.Handle("/metrics", promhttp.Handler())
	go func() {
		err = http.ListenAndServe(fmt.Sprintf(":%d", 8080), nil)
		if err != nil {
			zap.L().Fatal("error starting http server", zap.Error(err))
		}
	}()

	db, err := sql.Open("postgres", "host=localhost port=6432 user=postgres password=password dbname=telegram-bot sslmode=disable")
	if err != nil {
		zap.L().Fatal("could not connect to the database", zap.Error(err))
	}

	cbrClient := cbr.NewClient()

	expenseStorage := storage.NewExpenseStorage(db)
	ratesStorage := storage.NewRateStorage(db)
	userStorage := storage.NewUserStorage(db)

	ratesCollectorService := services.NewRatesCollectorService(cbrClient, ratesStorage, config)

	ratesCollectorService.CollectRates(ctx)

	tgClient, err := tg.New(config)
	if err != nil {
		zap.L().Fatal("tg client init failed", zap.Error(err))
	}

	msgModel := messages.New(tgClient, expenseStorage, userStorage, ratesStorage, config, ratesCollectorService)

	tgClient.ListenUpdates(ctx, msgModel)

	zap.L().Info("bot gracefully stopped")
}
