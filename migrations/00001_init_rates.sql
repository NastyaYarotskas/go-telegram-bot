-- +goose Up
-- +goose StatementBegin
create table rate
(
    id      INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    code    VARCHAR(3) NOT NULL,
    nominal INT        NOT NULL,
    value   TEXT       NOT NULL,
    date    TIMESTAMP  NOT NULL
);

-- так как много поиска по связке (code, date) добвляем индекс + unique для того, чтобы не хранить дубли
CREATE UNIQUE INDEX rate_code_date_uindex ON rate (code, date);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP INDEX rate_code_date_uindex;
DROP TABLE IF EXISTS rate;
-- +goose StatementEnd
