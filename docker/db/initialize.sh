pg_restore \
  --dbname=telegram-bot \
  --clean \
  --username=postgres \
  /tmp/db_dump.tar
